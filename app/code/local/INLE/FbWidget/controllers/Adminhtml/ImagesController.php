<?php

/**
 * Created by PhpStorm.
 * User: inle
 * Date: 17.04.17
 * Time: 5:43
 */
class INLE_FbWidget_Adminhtml_ImagesController extends Mage_Adminhtml_Controller_Action
{

    public function indexAction()
    {
        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('FbWidget/adminhtml_images'));
        $this->renderLayout();
    }

    public function testAction()
    {
        $image = Mage::getModel('FbWidget/image');
        var_dump($image);
    }

    public function newAction()
    {
        $this->_forward('edit');
    }

    public function editAction()
    {
        $id = $this->getRequest()->getParam('image_id');
        Mage::register('FbWidget_image', Mage::getModel('FbWidget/image')->load($id));

        $blockObject = (array)Mage::getSingleton('adminhtml/session')->getBlockObject(true);

        if (count($blockObject)) {
            Mage::registry('FbWidget_image')->setData($blockObject);
        }

        $this->loadLayout();
        $this->_addContent($this->getLayout()->createBlock('FbWidget/adminhtml_images_edit'));
        $this->renderLayout();
    }

    public function saveAction()
    {
        try {
            $id = $this->getRequest()->getParam('image_id');
            $banner = Mage::getModel('FbWidget/image')->load($id);
            $banner->setData($this->getRequest()->getParams())
                ->setCreatedAt(Mage::app()->getLocale()->date())
                ->save();
            if (!$banner->getId()) {
                Mage::getSingleton('adminhtml/session')->addError('Cannot save the banner');
            }

        } catch (Exception $e) {
            Mage::logException($e);
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            Mage::getSingleton('adminhtml/session')->setBlockObject($banner->getData());
            return $this->_redirect('*/*/edit', array('image_id' => $this->getRequest()->getParam('image_id')));
        }

        Mage::getSingleton('adminhtml/session')->addSuccess('Banner was saved successfully!');

        $this->_redirect('*/*/' . $this->getRequest()->getParam('back', 'index'),
            array('image_id' => $banner->getId()));
    }

    public function deleteAction()
    {
        $banner = Mage::getModel('FbWidget/image')
            ->setId($this->getRequest()->getParam('image_id'))
            ->delete();
        if ($banner->getId()) {
            Mage::getSingleton('adminhtml/session')->addSuccess('Banner was deleted successfully!');
        }
        $this->_redirect('*/*/');

    }
    public function massStatusAction()
    {
        $statuses = $this->getRequest()->getParams();

        try {
            $banners = Mage::getModel('FbWidget/image')
                ->getCollection()
                ->addFieldToFilter('image_id', array('in' => $statuses['massaction']));
            /** @var INLE_FbWidget_Model_Image $banner */
            foreach ($banners as $banner) {
                $banner
                    ->setData('image_status', $statuses['image_status'])
                    ->save();
            }
        } catch (Exception $e) {
            Mage::logException($e);
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            return $this->_redirect('*/*/');
        }
        Mage::getSingleton('adminhtml/session')->addSuccess('Banner were updated!');

        return $this->_redirect('*/*/');

    }
    public function massDeleteAction()
    {
        $banners = $this->getRequest()->getParams();
        try {
            $banners = Mage::getModel('FbWidget/image')
                ->getCollection()
                ->addFieldToFilter('image_id', array('in' => $banners['massaction']));
            foreach ($banners as $banner) {
                $banner->delete();
            }
        } catch (Exception $e) {
            Mage::logException($e);
            Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
            return $this->_redirect('*/*/');
        }
        Mage::getSingleton('adminhtml/session')->addSuccess('Banner were deleted!');

        return $this->_redirect('*/*/');

    }

    protected function _uploadImage($uploaded) {
        die($uploaded);
    }


}