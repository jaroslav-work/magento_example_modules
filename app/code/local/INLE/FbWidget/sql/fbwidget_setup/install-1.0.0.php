<?php
/**
 * Created by PhpStorm.
 * User: inle
 * Date: 17.04.17
 * Time: 5:11
 */

/** @var Mage_Core_Model_Resource_Setup $installer */
$installer = $this;
$installer->startSetup();


$table = $installer->getConnection()
    ->newTable($this->getTable('FbWidget/image'))
    ->addColumn('image_id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity' => true,
        'unsigned' => true,
        'nullable' => false,
        'primary' => true
    ))
    ->addColumn('title', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
        'nullable' => false
    ))
    ->addColumn('banner_url', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
        'nullable' => false
    ))
    ->addColumn('image_src', Varien_Db_Ddl_Table::TYPE_VARCHAR, null, array(
        'nullable' => false
    ))
    ->addColumn('image_status', Varien_Db_Ddl_Table::TYPE_TINYINT, null, array(
        'nullable' => false
    ))
    ->addColumn('created_at', Varien_Db_Ddl_Table::TYPE_DATETIME, null, array(
        'nullable' => false
    ));

if (!$installer->tableExists('FbWidget/image')) {
    $installer->getConnection()->createTable($table);
} 