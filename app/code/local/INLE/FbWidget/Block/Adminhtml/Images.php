<?php
/**
 * Created by PhpStorm.
 * User: inle
 * Date: 17.04.17
 * Time: 6:13
 */
class INLE_FbWidget_Block_Adminhtml_Images extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    public function __construct()
    {
        $this->_controller = 'adminhtml_images';
        $this->_blockGroup = 'FbWidget';
        $this->_headerText = Mage::helper('FbWidget')->__('Full based Widget');
        $this->_addButtonLabel = Mage::helper('FbWidget')->__('Add New Banner');
        parent::__construct();
    }
}