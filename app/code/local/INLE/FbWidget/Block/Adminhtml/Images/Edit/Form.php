<?php

/**
 * Created by PhpStorm.
 * User: inle
 * Date: 17.04.17
 * Time: 8:13
 */
class INLE_FbWidget_Block_Adminhtml_Images_Edit_Form extends Mage_Adminhtml_Block_Widget_Form
{

    /**
     * Init form
     */
    public function __construct()
    {
        parent::__construct();
        $this->setId('banner_form');
    }

    protected function _prepareForm()
    {
        $model = Mage::registry('FbWidget_image');

        $form = new Varien_Data_Form(
            array(
                'id' => 'edit_form',
                'action' => $this->getUrl('*/*/save', array('image_id' => $this->getRequest()->getParam('image_id'))),
                'method' => 'post',
                'enctype' => 'multipart/form-data'
            )
        );

        $form->setHtmlIdPrefix('banner_');



        $fieldset = $form->addFieldset('base_fieldset',
            array('legend' => Mage::helper('FbWidget')->__('General Information'), 'class' => 'fieldset-wide'));

        if ($model->getBlockId()) {
            $fieldset->addField('image_id', 'hidden', array(
                'name' => 'image_id',
            ));
        }

        $fieldset->addField('image_file', 'image', array(
            'label'     => Mage::helper('FbWidget')->__('Banner image'),
            'required'  => true,
            'name'      => 'image_file',
        ));

        $fieldset->addField('image_src', 'hidden', array(
            'label'     => Mage::helper('FbWidget')->__('Banner image'),
            'required'  => false,
            'name'      => 'image_src',
        ));

        $fieldset->addField('image_status', 'select', array(
            'label' => Mage::helper('FbWidget')->__('Status'),
            'title' => Mage::helper('FbWidget')->__('Status'),
            'name' => 'image_status',
            'required' => false,
            'options'   => Mage::getModel('FbWidget/source_status')->toArray(),
        ));

        $fieldset->addField('title', 'text', array(
            'name' => 'title',
            'label' => Mage::helper('FbWidget')->__('Title / IMG ALT'),
            'title' => Mage::helper('FbWidget')->__('Title / IMG ALT'),
            'required' => true,
        ));

        $fieldset->addField('banner_url', 'text', array(
            'name' => 'banner_url',
            'label' => Mage::helper('FbWidget')->__('Url / Banner Link'),
            'title' => Mage::helper('FbWidget')->__('Url / Banner Link'),
            'required' => true,

        ));

        $data = $model->getData();
        $data['image_file'] = $model->getImage();
        $form->setValues($data);
        $form->setUseContainer(true);
        $this->setForm($form);

        return parent::_prepareForm();
    }
}