<?php
/**
 * Created by PhpStorm.
 * User: inle
 * Date: 17.04.17
 * Time: 8:18
 */
class INLE_FbWidget_Block_Adminhtml_Images_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        $this->_objectId = 'image_id';
        $this->_controller = 'adminhtml_images';
        $this->_blockGroup = 'FbWidget';

        parent::__construct();

        $this->_updateButton('save', 'label', Mage::helper('FbWidget')->__('Save Banner'));
        $this->_updateButton('delete', 'label', Mage::helper('FbWidget')->__('Delete Banner'));

        $this->_addButton('saveandcontinue', array(
            'label'     => Mage::helper('adminhtml')->__('Save and Continue Edit'),
            'onclick'   => 'saveAndContinueEdit()',
            'class'     => 'save',
        ), -100);

        $this->_formScripts[] = "


            function saveAndContinueEdit(){
                editForm.submit($('edit_form').action+'back/edit/');
            }
        ";
    }

    /**
     * Get edit form container header text
     *
     * @return string
     */
    public function getHeaderText()
    {
        if (Mage::registry('FbWidget_image')->getId()) {
            return Mage::helper('FbWidget')->__("Edit Banner '%s'", $this->escapeHtml(Mage::registry('FbWidget_image')->getTitle()));
        }
        else {
            return Mage::helper('FbWidget')->__('New Banner');
        }
    }
}