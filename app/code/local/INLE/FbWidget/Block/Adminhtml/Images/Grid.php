<?php
/**
 * Created by PhpStorm.
 * User: inle
 * Date: 17.04.17
 * Time: 6:16
 */
class INLE_FbWidget_Block_Adminhtml_Images_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    public function __construct()
    {
        parent::__construct();
        $this->setId('cmsBlockGrid');
        $this->setDefaultSort('block_identifier');
        $this->setDefaultDir('ASC');
    }

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('FbWidget/image')->getCollection();
        /* @var $collection Mage_Cms_Model_Mysql4_Block_Collection */
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {
        $this->addColumn('image_src', array(
                'header'    => Mage::helper('FbWidget')->__('Banner image'),
                'align'     => 'left',
                'index'     => 'image',
                'filter'    => false, 
                'sortable'  => false,
                'renderer'  => 'INLE_FbWidget_Block_Adminhtml_Images_Grid_Renderer_Image',
        ));
        
        $this->addColumn('title', array(
            'header'    => Mage::helper('FbWidget')->__('Title / Alt'),
            'align'     => 'left',
            'index'     => 'title',
        ));

        $this->addColumn('banner_url', array(
            'header'    => Mage::helper('FbWidget')->__('Url / Link'),
            'align'     => 'left',
            'index'     => 'banner_url',
        ));

        $this->addColumn('image_status', array(
            'header'    => Mage::helper('cms')->__('Status'),
            'align'     => 'left',
            'type'      => 'options',
            'options'   => Mage::getModel('FbWidget/source_status')->toArray(),
            'index'     => 'image_status'
        ));

        $this->addColumn('image_status', array(
            'header'    => Mage::helper('cms')->__('Status'),
            'align'     => 'left',
            'type'      => 'options',
            'options'   => Mage::getModel('FbWidget/source_status')->toArray(),
            'index'     => 'image_status'
        ));


        $this->addColumn('created_at', array(
            'header'    => Mage::helper('FbWidget')->__('Created At'),
            'index'     => 'created_at',
            'type'      => 'date',

        ));


        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('image_id');
        $this->getMassactionBlock()->setIdFieldName('image_id');
        $this->getMassactionBlock()
            ->addItem('delete',
                array(
                    'label' => Mage::helper('FbWidget')->__('Delete'),
                    'url' => $this->getUrl('*/*/massDelete'),
                    'confirm' => Mage::helper('FbWidget')->__('Are you sure?')
                )
            )
            ->addItem('status',
                array(
                    'label' => Mage::helper('FbWidget')->__('Update status'),
                    'url' => $this->getUrl('*/*/massStatus'),
                    'additional' =>
                        array('image_status'=>
                            array(
                                'name' => 'image_status',
                                'type' => 'select',
                                'class' => 'required-entry',
                                'label' => Mage::helper('FbWidget')->__('Status'),
                                'values' => Mage::getModel('FbWidget/source_status')->toOptionArray()
                            )
                        )
                )
            );

        return $this;
    }

    /**
     * Row click url
     *
     * @return string
     */
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('image_id' => $row->getId()));
    }

}