<?php
/**
 * Created by PhpStorm.
 * User: inle
 * Date: 17.04.17
 * Time: 10:56
 */
class INLE_FbWidget_Block_Adminhtml_Images_Grid_Renderer_Image
    extends Mage_Adminhtml_Block_Widget_Grid_Column_Renderer_Abstract
{
    /**
     * @param Varien_Object $row
     */
    public function render(Varien_Object $row) #именно в этом методе необходимо добавлять логику
    {
        /** @var INLE_SbWidget_Helper_Data $helper */
        $helper = Mage::helper('FbWidget');
        /** @var INLE_FbWidget_Model_Image $row */
        $html = "<img src='{$row->getImage()}' width='100' height='auto'>";
        return $html;
    }
}