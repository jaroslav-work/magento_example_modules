<?php

/**
 * Created by PhpStorm.
 * User: inle
 * Date: 17.04.17
 * Time: 4:47
 */
class INLE_FbWidget_Model_Image extends Mage_Core_Model_Abstract
{
    protected $_config;
    public $image_file = 1;

    public function _construct()
    {
        parent::_construct();
        /** @var INLE_SbWidget_Helper_Data $helper */
        $helper = Mage::helper('FbWidget');
        $this->_init('FbWidget/image'); //Все в соотвествии с указанными в config.xml параметрами
        $this->_config = $helper->getConfig();
    }

    public function getImage()
    {

        if ($image = $this->getData('image_src')) {
            return Mage::getBaseUrl('media') . $image;
        } else {
            return '';
        }
    }

    protected function _beforeSave()
    {
        if ($this->getData('image_file/delete')) {
            $this->unsetImage();
        }

        try {
            $uploader = new Varien_File_Uploader('image_file');
            $uploader->setAllowedExtensions(array('jpg', 'jpeg', 'gif', 'png'));
            $uploader->setAllowCreateFolders(true);
            $uploader->setAllowRenameFiles(true);
            $this->setImage($uploader);

        } catch (Exception $e) {

        }
        return parent::_beforeSave();
    }

    /**
     * @param $image
     * @return $this
     * @throws Exception
     */
    public function setImage($image)
    {
        $root_path = Mage::getBaseDir('media');
        $media_path = $this->_config->media_path;

        if ($image instanceof Varien_File_Uploader) {
            /** @var Varien_File_Uploader $image */
            $image->save($root_path . DS . $media_path . DS);
            $image_name = $image->getUploadedFileName();
            $this->setData('image_src', $media_path .'/'. $image_name);
        }

        return $this;
    }

    public function unsetImage()
    {
        $image_src = $this->getData('image_src');
        @unlink(Mage::getBaseDir('media') . DS . $image_src);
        return $this;
    }

}
