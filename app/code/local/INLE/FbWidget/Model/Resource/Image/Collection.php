<?php

/**
 * Created by PhpStorm.
 * User: inle
 * Date: 17.04.17
 * Time: 5:00
 */
class INLE_FbWidget_Model_Resource_Image_Collection extends Mage_Core_Model_Resource_Db_Collection_Abstract
{

    public function _construct()
    {
        parent::_construct();
        $this->_init('FbWidget/image');
    }
}
