<?php
/**
 * Created by PhpStorm.
 * User: inle
 * Date: 07.04.17
 * Time: 20:04
 */

class INLE_SbWidget_Helper_Data extends Mage_Core_Helper_Abstract {

    /**
     * @return object
     */
    public function getConfig() {
        return (object) [
            'media_path' => Mage::getStoreConfig('SbWidget/settings/media_path'),
            'img_src' => Mage::getBaseUrl('media') . Mage::getStoreConfig('SbWidget/settings/upload_file')
        ];        
    }


}