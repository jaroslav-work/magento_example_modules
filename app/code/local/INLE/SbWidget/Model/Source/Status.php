<?php
/**
 * Created by PhpStorm.
 * User: inle
 * Date: 07.04.17
 * Time: 22:23
 */

class INLE_SbWidget_Model_Source_Status
{
    const ENABLED = '1';
    const DISABLED = '0';

    /**
     * Options getter
     *
     * @return array
     */
    public function toOptionArray()
    {
        return array(
            array('value' => self::ENABLED, 'label' => Mage::helper('SbWidget')->__('Enabled')),
            array('value' => self::DISABLED, 'label' => Mage::helper('SbWidget')->__('Disabled')),
        );
    }

    /**
     * Get options in "key-value" format
     *
     * @return array
     */
    public function toArray()
    {
        return array(
            self::DISABLED => Mage::helper('SbWidget')->__('Disabled'),
            self::ENABLED => Mage::helper('SbWidget')->__('Enabled'),
        );
    }

}