<?php
/**
 * Created by PhpStorm.
 * User: inle
 * Date: 17.04.17
 * Time: 1:59
 */
class INLE_SbWidget_Model_Custom_Path extends Mage_Core_Model_Config_Data{

    protected function _beforeSave() {
        $value = $this->getValue();
        $this->setValue(rtrim(ltrim($value, '/'),'/'));
        return $this;
    }
}