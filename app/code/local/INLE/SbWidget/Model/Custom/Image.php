<?php
class INLE_SbWidget_Model_Custom_Image extends Mage_Adminhtml_Model_System_Config_Backend_File{
    
    protected function _getUploadDir()
    {
        $path = Mage::getStoreConfig('SbWidget/settings/media_path');
        $uploadRoot = Mage::getBaseDir('media');

        if (empty($path)) {
            Mage::throwException(Mage::helper('catalog')->__('The base directory to upload file is not specified.'));
        }

        $uploadDir = (string) $path;
        return $uploadRoot . '/' . $uploadDir;
    }

    protected function _prependScopeInfo($path)
    {
        $mediaPath = Mage::getStoreConfig('SbWidget/settings/media_path');
        return $mediaPath. '/' .$path;
    }

    protected function _appendScopeInfo($path)
    {
        $mediaPath = Mage::getStoreConfig('SbWidget/settings/media_path');
        return $mediaPath . '/' . $path;
    }

    protected function _addWhetherScopeInfo()
    {
        return true;
    }
    
    protected function _escapeSlashes($str) {
        $slash = '/';
        return ltrim(rtrim($str, $slash), $slash);
    }



}